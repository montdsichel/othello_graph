// page scroll
$("a[href='#top']").click(function(){
  $("html, body").animate({scrollTop:0},"slow");
  return false;
});
// DOM control
$(function(){
	// windows-meiryo box-hight sizeControl
	var ua = navigator.userAgent;
	var mac = ua.match(/Mac|PPC/);
	if(!mac){
		$('#mainContents .recommendList dt').addClass('meiryo');
	}
	// new entry list DOM control
	var recentList = $('.recent-article-image li');
	// それぞれのli内で処理を繰り返す
	$(recentList).each(function(){
		// li内のa要素をラップ
		var txtWrapClass = $(this).children('a').wrap('<div class="headLine"></div>');
		// li内のimg要素をラップ
		var imgWrapClass = $(this).find('img').wrap('<div class="thumbs"><span class="photoMaskArc_s"></span></div>');
		// ラップしたimg要素の親dev.headLineのclassを変更
		$(imgWrapClass).closest('div.headLine').removeClass('headLine').addClass('imgBox');
		// ラップしたimgの次に出現する兄弟要素にclassを追加
		$(imgWrapClass).next('headLine').addClass('headLine_s');
	});
	// pager paging-number hidden
	$('ol.paging-number li:contains(...)').remove();
	// categories subTitle add
	var categories = [
		{main:'NEWS',sub:'最新のお知らせ'},
		{main:'SPECIAL',sub:'特集・イベント'},
		{main:'HUMAN',sub:'オセロ・インタビュー'},
		{main:'SPOT',sub:'ブロック紹介'},
		{main:'COMPETITION',sub:'メジャー大会特集'},
		{main:'OTHER',sub:'その他記事'}
	];
	$.each(categories,function(key,value){
		var title = $('#categoriesPage .categories h1 .title').text();
		if(title==value.main){
			$('#categoriesPage .categories h1 .subTitle').text(' / ' + value.sub);
			return false;
		}
	});
	// article main img replace
	var targetImg = $('#jsArticleMainImg');
	var targetPlace = $('#articlePage h1.title');
	targetImg.insertAfter(targetPlace);
	// copyright write
	var nowYear = new Date();
	$("span.copyright .year").text(nowYear.getFullYear());
});
