// page scroll
$("a[href='#top']").click(function(){
  $("html, body").animate({scrollTop:0},"slow");
  return false;
});

// pager DOM control
$(function(){
	// new entry list DOM control
	var textWrap = $('.recent-article-image li a').wrap('<div class="headLine"></div>');
	var imgWrap = $('.recent-article-image li a img').wrap('<div class="thumbs"><span class="photoMaskArc_s"></span></div>');
	imgWrap.closest('div.headLine').removeClass('headLine').addClass('imgBox').next('div.headLine').addClass('headLine_s');
	// article main img replace
	var targetImg = $('#jsArticleMainImg');
	var targetPlace = $('#articlePage h1.title');
	targetImg.insertAfter(targetPlace);
});
